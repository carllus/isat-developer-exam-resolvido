﻿using ISAT.Developer.Exam.Web.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ISAT.Developer.Exam.Web.Controllers
{
    public class UsuariosController : Controller
    {
        private readonly Contexto _contexto;

        public UsuariosController(Contexto contexto)
        {
            _contexto = contexto;
        }
        public async Task<IActionResult> Index()
        {
            return View(await _contexto.Usuarios.ToListAsync());
        }
        [HttpGet]
        public IActionResult CriarUsuario()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> CriarUsuario(Usuario usuario)
        {
            if(ModelState.IsValid)
            {
                //Usuario a = _contexto.Usuarios.ToList().Find(a => a.Email == usuario.Email);
                if (_contexto.Usuarios.ToList().Find(a => a.Email == usuario.Email) != null)
                {
                    TempData["msg"] = "<script>alert('o e-mail já existe');</script>";
                    return View(usuario);
                }
                else if (_contexto.Usuarios.ToList().Find(a => a.Nome == usuario.Nome && a.SobreNome == usuario.SobreNome) != null)
                {
                    TempData["msg"] = "<script>alert('o nome e sobrenome já existem para outro usuário');</script>";
                    return View(usuario);
                }
                else if (usuario.Idade < 10 || usuario.Idade > 100)
                {
                    TempData["msg"] = "<script>alert('a idade é inválida');</script>";
                    return View(usuario);
                }
                else if (usuario.Nome.Length > 255 || usuario.SobreNome.Length > 255 || usuario.Email.Length > 255)
                {
                    TempData["msg"] = "<script>alert('o limite de caracteres foi atingido');</script>";
                    return View(usuario);
                }
                else
                {
                    _contexto.Add(usuario);
                    await _contexto.SaveChangesAsync();
                    return RedirectToAction(nameof(Index));
                }
            }
            else
            {
                return View(usuario);
            }
        }

        [HttpGet]
        public IActionResult AtualizarUsuario(int? id)
        {
            if(id!=null)
            {
                Usuario usuario = _contexto.Usuarios.Find(id);
                return View(usuario);
            }
            else
            {
                return NotFound();
            }
        }

        [HttpPost]
        public async Task<IActionResult> AtualizarUsuario(int? id, Usuario usuario)
        {
            if(id!=null)
            {
                if (ModelState.IsValid)
                {
                    _contexto.Update(usuario);
                    await _contexto.SaveChangesAsync();
                    return RedirectToAction(nameof(Index));
                }
                return View(usuario);
            }
            else
            {
                return NotFound();
            }
        }

        [HttpGet]
        public IActionResult ExcluirUsuario(int? id)
        {
            if (id != null)
            {
                Usuario usuario = _contexto.Usuarios.Find(id);
                return View(usuario);
            }
            else
            {
                return NotFound();
            }
        }

        [HttpPost]
        public async Task<IActionResult> ExcluirUsuario(int? id, Usuario usuario)
        {
            if (id != null)
            {
                _contexto.Remove(usuario);
                await _contexto.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            else
            {
                return NotFound();
            }
        }
    }
}
