using ISAT.Developer.Exam.Infrastructure.Utilities;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace ISAT.Developer.Exam.Infrastructure.ORM.Contexts
{
    public class EFContextDB : DbContext
    {
        #region properties

        #endregion

        #region ctor

        public EFContextDB(DbContextOptions<EFContextDB> options) : base(options)
        {
        }

        public EFContextDB() { }

        #endregion

        #region dbsets

        #endregion

        #region methods
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }

        public override Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess, CancellationToken cancellationToken = default)
        {
            return base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(GetConnectionStringUtility.GetConnectionString("SQLConnection"));
        }

        #endregion

    }
}